# 20200924-BoobalanMunusamy-NYCSchools

NYC School Android applicaiton  by Boobalan Munusamy

Android applicaiton helps your to browse the NYC school list. Also, it allows you to see the
SAT Result information

Key Features:
-------------
1. See the list of Schools
2. Search the schools by name, location or PIN code
3. Schools list can be sorted by A->Z or Z-A

Support
-------

1. Min SDK 23- Android 6.0 and Above
2. Local database support
3. Only internet permission is required

Android feature demonstrated:
------------------------------
1. Room Database = To store the list offline
2. WorkManager = To download the schools in the chunk of 50 schools per request
3. Pagination = To fetch the data from Room DB and showing it recycler view
4. NavigationUI = Navigations
5. OkHttpClient = For network call
6. GSON  = for Json conversion
7. Material Theme = For theming

Test case covered:
------------------
1. Repositry or DAO insert & fetch operation
2. Fetch SATResult network call
3. ViewModel & Worker

Languages:
----------
Java = Views(Fragment, Activity) & View Model
Kotlin = Data, Worker & Network calls



