/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */


package com.bob.a20200924_boobalanmunusamy_nycschools.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.bob.a20200924_boobalanmunusamy_nycschools.R;
import com.bob.a20200924_boobalanmunusamy_nycschools.worker.DownloadSchoolWorker;

// Main activity to handle the home screen
public class MainActivity extends AppCompatActivity {
    OneTimeWorkRequest mOneTimeWorkerRequest = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initIU();
    }

    void initIU() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mOneTimeWorkerRequest = new OneTimeWorkRequest.Builder(DownloadSchoolWorker.class).build();
        WorkManager.getInstance(this).enqueue(mOneTimeWorkerRequest);

        NavHostFragment mNavHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        if (mNavHostFragment != null) {
            NavController navController = mNavHostFragment.getNavController();
            NavGraph navGraph = navController.getNavInflater().inflate(R.navigation.nav_home);
            navGraph.setStartDestination(R.id.schoolListFragment);
            //set the nav bar
            navController.setGraph(navGraph);
            //setup the app bar configuration
            AppBarConfiguration appBar = new AppBarConfiguration.Builder(R.id.schoolListFragment).build();
            NavigationUI.setupActionBarWithNavController(this, navController, appBar);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Cancel the request
        if (mOneTimeWorkerRequest != null) {
            WorkManager.getInstance(this).cancelWorkById(mOneTimeWorkerRequest.getId());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}