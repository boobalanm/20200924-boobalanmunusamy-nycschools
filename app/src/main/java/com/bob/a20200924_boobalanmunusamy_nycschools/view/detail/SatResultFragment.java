/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.view.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.bob.a20200924_boobalanmunusamy_nycschools.R;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo;

public class SatResultFragment extends Fragment {

    private SatResultViewModel mViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sat_result_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(SatResultViewModel.class);
        init();
    }

    void init() {
        if (getArguments() != null) {
            SchoolInfo schoolInfo = SatResultFragmentArgs.fromBundle(getArguments()).getSchoolInfo();
            mViewModel.makeServiceCall(schoolInfo);
            if (getView() == null) return;

            TextView schoolName = getView().findViewById(R.id.tvName);
            TextView location = getView().findViewById(R.id.tvLocation);
            TextView cellPhone = getView().findViewById(R.id.tvCellNumber);
            TextView email = getView().findViewById(R.id.tvEmail);
            ImageView callIcon = getView().findViewById(R.id.ivDial);
            callIcon.setOnClickListener(view -> {

                // Make a phone call
                if (schoolInfo.getPhone() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel: " + schoolInfo.getPhone().replace("-", "")));
                    startActivity(intent);
                }

            });
            ImageView emailIcon = getView().findViewById(R.id.ivEmail);
            emailIcon.setOnClickListener(view -> {

                // Compose the email
                if (schoolInfo.getEmail() != null) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:" + schoolInfo.getEmail()));
                    startActivity(Intent.createChooser(intent, getString(R.string.msg_send_email)));

                }

            });

            schoolName.setText(schoolInfo.getName());
            if (schoolInfo.getLocation() != null) {
                int gpsIndex = schoolInfo.getLocation().indexOf(" (");
                location.setText(schoolInfo.getLocation().substring(0, gpsIndex));
            }
            cellPhone.setText(schoolInfo.getPhone());
            email.setText(schoolInfo.getEmail());


            // Update test taker
            View totalLayout = getView().findViewById(R.id.slTotal);
            TextView title = totalLayout.findViewById(R.id.tvExamName);
            title.setText(getString(R.string.test_take));
            TextView tlTotalTestTaken = totalLayout.findViewById(R.id.tvScore);


            // To update Reading
            View readingLayout = getView().findViewById(R.id.slReading);
            TextView readTitle = readingLayout.findViewById(R.id.tvExamName);
            readTitle.setText(getString(R.string.avg_reading));
            TextView tlAvgReading = readingLayout.findViewById(R.id.tvScore);

            // To update writing
            View writingLayout = getView().findViewById(R.id.slWriting);
            TextView writeTitle = writingLayout.findViewById(R.id.tvExamName);
            writeTitle.setText(getString(R.string.avg_writing));
            TextView tlAvgWriting = writingLayout.findViewById(R.id.tvScore);


            // To update writing
            View mathLayout = getView().findViewById(R.id.slMath);
            TextView mathTitle = mathLayout.findViewById(R.id.tvExamName);
            mathTitle.setText(getString(R.string.avg_math));
            TextView tlAvgMath = mathLayout.findViewById(R.id.tvScore);

            mViewModel.satResultLiveData.observe(getViewLifecycleOwner(), satResult -> {
                if (getView() != null) {

                    setScores(tlTotalTestTaken, satResult.getTestTaker());
                    setScores(tlAvgReading, satResult.getReadingScore());
                    setScores(tlAvgWriting, satResult.getWriteAvgScore());
                    setScores(tlAvgMath, satResult.getMathAvgScore());
                }
            });
        }
    }

    void setScores(TextView textView, String value) {
        String NA = getString(R.string.na);

        if (value == null || value.isEmpty()) {
            textView.setText(NA);
        } else {
            textView.setText(value);
        }
    }

}