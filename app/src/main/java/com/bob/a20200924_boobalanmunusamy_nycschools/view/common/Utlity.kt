/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.view.common

import android.content.Context
import android.content.SharedPreferences

class Utility {

    private val PREF_FILE_NAME = "School_Shared_Pref"
    lateinit var mContext: Context

    fun getSharedPreference(): SharedPreferences {
        return mContext.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
    }

    fun storeData(key: String, value: Any) {
        val sharedPreferences = getSharedPreference()
        with(sharedPreferences.edit()) {
            when (value) {
                is Int -> {
                    putInt(key, value)
                }
                is Boolean -> {
                    putBoolean(key, value)
                }
                is String -> {
                    putString(key, value)
                }
            }
            commit()
        }
    }


    companion object {
        @Volatile
        private var INSTANCE: Utility? = null

        fun getInstance(context: Context): Utility {
            val temp: Utility? = INSTANCE
            if (temp != null) {
                return temp
            }
            synchronized(this) {
                val instance = Utility()
                instance.mContext = context
                INSTANCE = instance
                return instance
            }
        }
    }

}