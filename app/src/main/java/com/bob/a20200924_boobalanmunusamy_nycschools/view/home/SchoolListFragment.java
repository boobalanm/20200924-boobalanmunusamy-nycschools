/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.view.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bob.a20200924_boobalanmunusamy_nycschools.R;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.common.FilterOption;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.common.SortingOrder;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo;

public class SchoolListFragment extends Fragment {

    private SchoolListViewModel mViewModel;

    private int previousMenuSelection;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.school_list_fragment, container, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

        inflater.inflate(R.menu.home_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                mViewModel.applyFilter(s);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_sort: {
                item.getSubMenu().findItem(previousMenuSelection).setChecked(true);
                break;
            }
            case R.id.menu_none: {
                item.setChecked(true);
                previousMenuSelection = item.getItemId();
                mViewModel.applyFilter(new FilterOption((SortingOrder.NONE), ""));
                break;
            }
            case R.id.menu_a_z: {
                item.setChecked(true);
                previousMenuSelection = item.getItemId();
                mViewModel.applyFilter(new FilterOption(SortingOrder.AZ, ""));
                break;
            }
            case R.id.menu_z_a: {
                item.setChecked(true);
                previousMenuSelection = item.getItemId();
                mViewModel.applyFilter(new FilterOption(SortingOrder.ZA, ""));
                break;
            }

        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        previousMenuSelection = R.id.menu_none;
        setHasOptionsMenu(true);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(SchoolListViewModel.class);
        initUI();
    }

    void initUI() {
        View view = getView();
        if (view != null) {
            RecyclerView recyclerView = view.findViewById(R.id.rvSchools);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            SchoolListAdapter schoolListAdapter = new SchoolListAdapter(getContext(), (view1, schoolInfo) -> navigateToDetails(schoolInfo));
            // attach the adapter to list view
            mViewModel.allSchoolInfo.observe(getViewLifecycleOwner(), schoolListAdapter::submitList);
            recyclerView.setAdapter(schoolListAdapter);
            recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            recyclerView.setItemViewCacheSize(20);

            ConstraintLayout mEmptyView = view.findViewById(R.id.clEmptyList);
            schoolListAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    checkEmpty();
                }

                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);
                    checkEmpty();
                }

                @Override
                public void onItemRangeRemoved(int positionStart, int itemCount) {
                    super.onItemRangeRemoved(positionStart, itemCount);
                    checkEmpty();
                }

                void checkEmpty() {
                    mEmptyView.setVisibility(schoolListAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
                }
            });
        }


    }


    void navigateToDetails(SchoolInfo schoolInfo) {
        NavDirections actionToSATSummary = SchoolListFragmentDirections.Companion.actionSchoolListFragmentToSatResultFragment(schoolInfo);
        NavHostFragment.findNavController(this).navigate(actionToSATSummary);
    }

}