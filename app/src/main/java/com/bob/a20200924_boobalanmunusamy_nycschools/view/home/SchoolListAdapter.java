/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.view.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bob.a20200924_boobalanmunusamy_nycschools.R;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo;

import org.jetbrains.annotations.NotNull;

class SchoolListAdapter extends PagedListAdapter<SchoolInfo, SchoolListAdapter.ViewHolder> {

    Context mContext;
    OnItemClickListener onItemClickListener;

    public SchoolListAdapter(Context context, OnItemClickListener itemClickListener) {
        this(new DiffUtil.ItemCallback<SchoolInfo>() {
            @Override
            public boolean areItemsTheSame(@NonNull SchoolInfo oldItem, @NonNull SchoolInfo newItem) {
                return oldItem.getDbn().equalsIgnoreCase(newItem.getDbn());
            }

            @Override
            public boolean areContentsTheSame(@NonNull SchoolInfo oldItem, @NonNull SchoolInfo newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.mContext = context;
        this.onItemClickListener = itemClickListener;
    }

    protected SchoolListAdapter(@NotNull DiffUtil.ItemCallback<SchoolInfo> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_school_info, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.onBind(position);
    }

    interface OnItemClickListener {
        void onItemClick(View view, SchoolInfo schoolInfo);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvPhone;
        TextView tvLocation;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvSchoolName);
            tvPhone = itemView.findViewById(R.id.tvMobileNumber);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            itemView.setOnClickListener(view -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, getItem(getAdapterPosition()));
                }
            });
        }

        void onBind(int position) {
            SchoolInfo schoolInfo = getItem(position);
            if (schoolInfo != null) {
                tvName.setText(schoolInfo.getName());
                if (schoolInfo.getLocation() != null) {
                    int gpsIndex = schoolInfo.getLocation().indexOf(" (");
                    tvLocation.setText(schoolInfo.getLocation().substring(0, gpsIndex));
                }
                tvPhone.setText(String.format(mContext.getString(R.string.call), schoolInfo.getPhone()));
                tvPhone.setOnClickListener(view -> {

                });
            }
        }


    }
}
