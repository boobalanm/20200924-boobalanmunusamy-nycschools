/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.view.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.bob.a20200924_boobalanmunusamy_nycschools.model.SchoolDatabase;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.common.FilterOption;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.common.SortingOrder;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.dao.SchoolDao;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.repository.SchoolRepository;

public class SchoolListViewModel extends AndroidViewModel {
    LiveData<PagedList<SchoolInfo>> allSchoolInfo;
    MutableLiveData<FilterOption> filterOptionMutableLiveData = new MutableLiveData<>(new FilterOption(SortingOrder.NONE, ""));

    public SchoolListViewModel(@NonNull Application application) {
        super(application);

        SchoolDao schoolDao = SchoolDatabase.Companion.getDataBase(application).schoolDao();
        SchoolRepository schoolRepository = new SchoolRepository(schoolDao);
        PagedList.Config config = new PagedList.Config.Builder().setEnablePlaceholders(true).setPageSize(50).build();
        allSchoolInfo = Transformations.switchMap(filterOptionMutableLiveData, input -> new LivePagedListBuilder<>(schoolRepository.getSchools(input), config).build());
    }

    public void applyFilter(FilterOption filterOption) {
        if (filterOptionMutableLiveData.getValue() != null) {
            filterOption.setQuery(filterOptionMutableLiveData.getValue().getQuery());
        }
        filterOptionMutableLiveData.postValue(filterOption);
    }

    public void applyFilter(String s) {
        FilterOption filterOption = filterOptionMutableLiveData.getValue();
        if (filterOption != null) {
            filterOption.setQuery(s);
            filterOptionMutableLiveData.postValue(filterOption);
        }
    }
}