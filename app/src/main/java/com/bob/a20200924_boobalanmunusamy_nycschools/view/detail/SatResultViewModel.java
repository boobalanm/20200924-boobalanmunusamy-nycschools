/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.view.detail;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SatResult;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class SatResultViewModel extends AndroidViewModel {

    MutableLiveData<SatResult> satResultLiveData = new MutableLiveData<>();

    public SatResultViewModel(@NonNull Application application) {
        super(application);

    }

    // Make service call to fetch SAT summary
    void makeServiceCall(SchoolInfo schoolInfo) {
        String url = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?$where=dbn=\"" + schoolInfo.getDbn() + "\"";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                Gson gson = new Gson();
                ResponseBody body = response.body();
                if (body != null) {
                    SatResult[] result = gson.fromJson(body.string(), SatResult[].class);
                    if (result.length > 0) {
                        satResultLiveData.postValue(result[0]);

                    }
                }
            }
        });
    }


}