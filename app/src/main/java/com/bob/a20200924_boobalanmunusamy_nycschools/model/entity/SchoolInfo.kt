/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.model.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

// Entity holds the SchoolInfo
@Parcelize
@Entity(tableName = "school_table")
data class SchoolInfo(@PrimaryKey @field:SerializedName("dbn") val dbn: String,
                      @field:SerializedName("school_name") val name: String?,
                      @field:SerializedName("phone_number") val phone: String?,
                      @field:SerializedName("location") val location: String?,
                      @field:SerializedName("school_email") val email: String?) : Parcelable