/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.model.repository

import androidx.paging.DataSource
import com.bob.a20200924_boobalanmunusamy_nycschools.model.common.FilterOption
import com.bob.a20200924_boobalanmunusamy_nycschools.model.common.SortingOrder
import com.bob.a20200924_boobalanmunusamy_nycschools.model.dao.SchoolDao
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo

class SchoolRepository(private val schoolDao: SchoolDao) {
    fun getSchools(filterOption: FilterOption): DataSource.Factory<Int, SchoolInfo> {
        if (filterOption.query == null) {
            filterOption.query = ""
        }
        return when (filterOption.sortingOrder) {
            SortingOrder.ZA -> {
                schoolDao.getSchoolsZA(filterOption.query!!)
            }
            SortingOrder.AZ -> {
                schoolDao.getSchoolsAZ(filterOption.query!!)
            }
            else -> {
                schoolDao.getSchools(filterOption.query!!)
            }
        }
    }

    fun insertAll(arrSchools: List<SchoolInfo>) {
        schoolDao.insertAll(arrSchools)
    }
}