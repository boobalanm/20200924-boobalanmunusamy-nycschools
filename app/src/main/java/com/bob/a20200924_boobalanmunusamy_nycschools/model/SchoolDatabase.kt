/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bob.a20200924_boobalanmunusamy_nycschools.model.dao.SchoolDao
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SatResult
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo

@Database(entities = [SchoolInfo::class, SatResult::class], version = 1, exportSchema = false)
abstract class SchoolDatabase : RoomDatabase() {

    abstract fun schoolDao(): SchoolDao
    companion object {
        @Volatile
        private var INSTANCE: SchoolDatabase? = null
        fun getDataBase(context: Context): SchoolDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        SchoolDatabase::class.java, "school_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}