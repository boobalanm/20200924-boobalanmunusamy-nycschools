/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.model.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo


@Dao
interface SchoolDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(schools: List<SchoolInfo>)

    // Get schools information from db
    @Query("SELECT * FROM school_table WHERE name LIKE '%' ||:search ||'%' OR location LIKE '%'||:search||'%'")
    fun getSchools(search: String): DataSource.Factory<Int, SchoolInfo>

    @Query("SELECT * FROM school_table WHERE name LIKE '%' ||:search ||'%' OR location LIKE '%'||:search||'%' ORDER BY name ASC ")
    fun getSchoolsAZ(search: String): DataSource.Factory<Int, SchoolInfo>

    @Query("SELECT * FROM school_table WHERE name LIKE '%' ||:search ||'%' OR location LIKE '%'||:search||'%' ORDER BY name DESC ")
    fun getSchoolsZA(search: String): DataSource.Factory<Int, SchoolInfo>

    @Query("DELETE from school_table")
    suspend fun clear()
}