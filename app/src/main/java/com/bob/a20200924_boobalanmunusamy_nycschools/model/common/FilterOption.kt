/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.model.common

// To hold the sorting oder
enum class SortingOrder {
    NONE, AZ, ZA
}

// Object holds the filter options
data class FilterOption(var sortingOrder: SortingOrder, var query: String?)