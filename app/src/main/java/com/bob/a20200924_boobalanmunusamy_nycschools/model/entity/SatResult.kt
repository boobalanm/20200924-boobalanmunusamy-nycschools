/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

// Entity hold the SAT Result
@Entity(tableName = "sat_result")
data class SatResult(@PrimaryKey val id: Long,
                     @field:SerializedName("dbn") val dbn: String,
                     @field:SerializedName("num_of_sat_test_takers") val testTaker: String?,
                     @field:SerializedName("sat_critical_reading_avg_score") val readingScore: String?,
                     @field:SerializedName("sat_math_avg_score") val mathAvgScore: String?,
                     @field:SerializedName("sat_writing_avg_score") val writeAvgScore: String?)