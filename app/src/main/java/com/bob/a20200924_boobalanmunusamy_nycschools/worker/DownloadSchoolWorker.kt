/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.worker

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.bob.a20200924_boobalanmunusamy_nycschools.model.SchoolDatabase
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo
import com.bob.a20200924_boobalanmunusamy_nycschools.model.repository.SchoolRepository
import com.bob.a20200924_boobalanmunusamy_nycschools.view.common.Utility
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request

class DownloadSchoolWorker(private val appContext: Context, workerParameters: WorkerParameters) :
        Worker(appContext, workerParameters) {
    val TAG = DownloadSchoolWorker::class.java.name

    override fun doWork(): Result {

        val PAGE_SIZE = 50
        val KEY_OFFSET = "KEY_OFFSET"
        var keepDownload = true
        while (keepDownload) {
            val sharedPreferences = Utility.getInstance(appContext).getSharedPreference()
            val offset = sharedPreferences.getInt(KEY_OFFSET, 0)
            Log.d(TAG, "Offset: $offset")
            val url = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?\$limit=$PAGE_SIZE&\$offset=$offset"
            val client = OkHttpClient()
            val request = Request.Builder().url(url).build()
            val body = client.newCall(request).execute().body?.string()
            Log.d(TAG, "Response: $body")

            if (body == null) {
                keepDownload = false
            }
            val arrSchoolInfo = Gson().fromJson(body!!, Array<SchoolInfo>::class.java).toList()
            if (arrSchoolInfo.isEmpty()) {
                keepDownload = false
            }
            // Save the data into DB
            val schoolDao = SchoolDatabase.getDataBase(appContext).schoolDao()
            val repository = SchoolRepository(schoolDao)
            repository.insertAll(arrSchoolInfo)
            Utility.getInstance(appContext).storeData(KEY_OFFSET, offset + arrSchoolInfo.size)
        }

        Log.d(TAG, "Stopped")
        return Result.success()


    }

}