/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.model.repository

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.paging.LivePagedListBuilder
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bob.a20200924_boobalanmunusamy_nycschools.model.SchoolDatabase
import com.bob.a20200924_boobalanmunusamy_nycschools.model.dao.SchoolDao
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

// To validate the DAO and Repository stuffs
@RunWith(AndroidJUnit4::class)
class SchoolRepositoryTest {
    lateinit var schoolDao: SchoolDao
    lateinit var context: Context

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        schoolDao = SchoolDatabase.getDataBase(context).schoolDao()
        val schoolInfoA = SchoolInfo("123", "A Texas University", "425-445-5565", "7344 Parkridge Blvd", "xxx@gmail.com")
        val schoolInfoB = SchoolInfo("124", "B Texas University", "425-445-5565", "7344 Parkridge Blvd", "xxx@gmail.com")
        val schoolInfoC = SchoolInfo("125", "C Texas University", "425-445-5565", "7344 Parkridge Blvd", "xxx@gmail.com")
        schoolDao.insertAll(arrayListOf(schoolInfoA, schoolInfoB, schoolInfoC))
    }


    // To test insert and fetch functionality
    @Test
    fun testSchoolWithQuery() {
        // fetch the data in default order
        val fetchSchoolInfo = schoolDao.getSchools("Texas")
        val liveData = LivePagedListBuilder(fetchSchoolInfo, 10).build()
        val pagedList = liveData.getOrAwaitValue()
        assertTrue(pagedList.snapshot().size > 0)
    }

    @Test
    fun testSchoolSortAZ() {
        //fetch the data ASC order
        val fetchSchoolInfo = schoolDao.getSchoolsAZ("")
        val liveData = LivePagedListBuilder(fetchSchoolInfo, 10).build()
        val pagedList = liveData.getOrAwaitValue().snapshot()
        if (pagedList.size > 2) {
            assertTrue(pagedList[0].name!!.startsWith("A", true))
            assertTrue(pagedList[1].name!!.startsWith("B", true))
            assertTrue(pagedList[2].name!!.startsWith("C", true))
        } else {
            fail("List is not available")
        }
    }

    @Test
    fun testSchoolSortZA() {
        //get the data from DES order
        val fetchSchoolInfo = schoolDao.getSchoolsZA("")
        val liveData = LivePagedListBuilder(fetchSchoolInfo, 10).build()
        val pagedList = liveData.getOrAwaitValue().snapshot()
        if (pagedList.size > 2) {
            assertTrue(pagedList[0].name!!.startsWith("C", true))
            assertTrue(pagedList[1].name!!.startsWith("B", true))
            assertTrue(pagedList[2].name!!.startsWith("A", true))
        } else {
            fail("List is not available")
        }
    }

    @After
    fun clearDB() {
        GlobalScope.launch {
            schoolDao.clear()
        }

    }

    /* Copyright 2019 Google LLC.
   SPDX-License-Identifier: Apache-2.0 */
    fun <T> LiveData<T>.getOrAwaitValue(
            time: Long = 2,
            timeUnit: TimeUnit = TimeUnit.SECONDS
    ): T {
        var data: T? = null
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(o: T?) {
                data = o
                latch.countDown()
                this@getOrAwaitValue.removeObserver(this)
            }
        }

        this.observeForever(observer)

        // Don't wait indefinitely if the LiveData is not set.
        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

        @Suppress("UNCHECKED_CAST")
        return data as T
    }
}