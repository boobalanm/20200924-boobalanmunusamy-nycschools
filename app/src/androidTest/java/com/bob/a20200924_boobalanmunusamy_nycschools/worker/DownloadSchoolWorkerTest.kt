/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.worker

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.work.ListenableWorker
import androidx.work.testing.TestListenableWorkerBuilder
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


// Worker to download the School list from the background
@RunWith(AndroidJUnit4::class)
class DownloadSchoolWorkerTest {
    private lateinit var context: Context
    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
    }

    @Test
    fun testSyncWorker() {
        val worker = TestListenableWorkerBuilder<DownloadSchoolWorker>(context).build()
        runBlocking {

            val result = worker.doWork()
            assertThat(result, `is`(ListenableWorker.Result.success()))
        }

    }
}