/*
 *
 *  * Copyright (c) 2020 .All rights are reserved by Boobalan Munusamy
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under
 *  * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *  * CONDITIONS OF ANY KIND, either express or implied.
 *
 *
 */

package com.bob.a20200924_boobalanmunusamy_nycschools.view.detail;

import android.app.Application;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SatResult;
import com.bob.a20200924_boobalanmunusamy_nycschools.model.entity.SchoolInfo;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class SatResultViewModelTest {
    @Rule
    public InstantTaskExecutorRule task = new InstantTaskExecutorRule();
    SatResultViewModel viewModel;
    SatResult networkResult;
    // To sync the asycn task
    CountDownLatch countDownLatch = new CountDownLatch(1);
    // Observe the changes
    Observer<SatResult> observer = new Observer<SatResult>() {
        @Override
        public void onChanged(SatResult satResult) {
            countDownLatch.countDown();
            networkResult = satResult;
        }
    };
    Application application;

    @Before
    public void setUp() {
        application = Mockito.mock(Application.class);
        viewModel = new SatResultViewModel(application);
    }

    @Test
    public void makeServiceCall() {
        viewModel.satResultLiveData.observeForever(observer);
        viewModel.makeServiceCall(new SchoolInfo("01M292", "", "", "", ""));
        try {
            // wait till the thread finish the task
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // observer would update this result
        assertNotNull(networkResult);
    }
}